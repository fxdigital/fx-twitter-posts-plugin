<?php
    /*
    *   Plugin Name: FX Twitter Posts
    *   Plugin URI: https://fxdigital.uk
    *   Description: Imports Tweets to a hidden custom post type called fx_twitter.
    *   Version: 1.0.7
    *   Author: FX Digital Ltd
    *   Author URI: https://fxdigital.uk
    *   License: GPL2
    */

// Parses the tweet for user mentions, hashtags and urls
function fx_twitter_parse_tweet($tweet) {
    // Convert urls to <a> links
    $tweet = preg_replace("/([\w]+\:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/", '<a target="_blank" rel="noreferrer noopener" href="$1">$1</a>', $tweet);

    // Convert # to twitter searches in <a> links
    $tweet = preg_replace("/#([A-Za-z0-9\/\.]*)/", '<a target="_blank" rel="noreferrer noopener" href="http://twitter.com/hashtag/$1?src=hash">#$1</a>', $tweet);

    // Convert @ to twitter profiles in <a> links
    $tweet = preg_replace("/@([A-Za-z0-9\/\.]*)/", '<a target="_blank" rel="noreferrer noopener" href="http://www.twitter.com/$1">@$1</a>', $tweet);

    return $tweet;
}

// Creates an import schedule to pull in tweets
function fx_twitter_create_import_schedule() {
    $timestamp = wp_next_scheduled( 'fx_twitter-auto-fetch' );

    if ($timestamp == false) :
        //Schedule the event for right now, then to repeat hourly
        wp_schedule_event( time(), 'hourly', 'fx_twitter-auto-fetch');
    endif;
}

// Attaches the function to cron-job
add_action('fx_twitter-auto-fetch', 'fx_posts_from_twitter');

// On deactivation, remove all functions from the scheduled action hook.
function fx_twitter_deactivation() {
    wp_clear_scheduled_hook( 'fx_twitter-auto-fetch' );
    fx_twitter_clear_settings();
}
register_deactivation_hook( __FILE__, 'fx_twitter_deactivation' );

// On plugin activation setup the custom post type and taxonomy
function fx_twitter_posts_init() {
    $user_name = 'FX Digital';
    $post_args = array(
            'description' => 'Internal post type to keep track of the' . $user_name . 'twitter account.',
            'exclude_from_search' => true,
            'labels' => array(
                'name' => 'Tweets',
                'singular_name' => 'Tweet',
            ),
            'public' => true,
            'supports' => array( 'title', 'editor', 'thumbnail' )
        );
    register_post_type('fx_twitter', $post_args);

    $tax_args = array(
        'description' => 'Taxonomy for fx_tweets screen names.',
        'exclude_from_search' => true,
        'labels' => array(
            'name' => 'Screen Names',
            'singular_name' => 'Screen Name',
            'all_items' => 'All Screen Names',
            'edit_item' => 'Edit Screen Name',
            'view_item' => 'View Screen Name',
            'update_item' => 'Updated Screen Name',
            'add_new_item' => 'Add Screen Name'
        ),
        'public' => false
    );

    register_taxonomy('fx_screen_names', 'fx_twitter' ,$tax_args);
}
add_action('init', 'fx_twitter_posts_init');

// Converts tweets into posts - fx_twitter post type
function fx_posts_from_twitter() {
    $access_token = get_option('fx_twitter_access_token');
    $screen_names = explode(',', get_option('fx_twitter_screen_names'));

    // Loop through each screen name
    foreach ($screen_names as $screen_name) :

        // Set since id to empty string for first query
        $since_id = '';
        $recent_tweet = new WP_Query(
            array(
                'post_type' => 'fx_twitter',
                'posts_per_page' => 1,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'fx_screen_names',
                        'field' => 'slug',
                        'terms' => $screen_name
                    )
                )
            )
        );

        if ($recent_tweet->have_posts()) :
            $since_id = $recent_tweet->post->post_title;
        endif;
        wp_reset_postdata();

        // Get tweets from Twitter API
        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=20&include_rts=1&exclude_replies=1&screen_name=' . trim($screen_name);

        if (!empty($since_id)) :
            $url .= '&since_id='.$since_id;
        endif;

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url);
        curl_setopt( $ch, CURLOPT_HTTPGET, 1);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $access_token,
                'Content-Type: application/json'
            )
        );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = json_decode(curl_exec( $ch ));

        // Import each tweet as a fx_twitter post. Tweets are referenced in the post content.
        foreach ($response as $tweet) :
            if (!isset($response->errors) && !fx_twitter_slug_exists($tweet->id)) :
                // Handling retweets
                if (isset($tweet->retweeted_status)) :
                    $post = array(
                        'post_type'     => 'fx_twitter',
                        'post_content'  => htmlentities($tweet->retweeted_status->text),
                        'post_excerpt'  => htmlentities($tweet->retweeted_status->text),
                        'post_date'     => date("Y-m-d H:i:s", strtotime($tweet->retweeted_status->created_at)),
                        'post_date_gmt' => date("Y-m-d H:i:s", strtotime($tweet->retweeted_status->created_at)),
                        'post_status'   => 'publish',
                        'post_title'    => $tweet->id,
                        'post_name'     => $tweet->id,
                        'meta_input'    => array(
                            'fx_twitter_id' => $tweet->retweeted_status->user->id,
                            'fx_twitter_name' => $tweet->retweeted_status->user->name,
                            'fx_twitter_screen_name' => $tweet->retweeted_status->user->screen_name,
                            'fx_twitter_profile_image' => $tweet->retweeted_status->user->profile_image_url_https
                        )
                    );

                    $new_post = wp_insert_post( $post, true );

                    wp_set_object_terms($new_post, $tweet->user->screen_name, 'fx_screen_names');

                    // If there is an image set it as the featured image on the new post
                    if (isset($tweet->retweeted_status->entities->media) && $new_post !== 0) :
                        fx_twitter_generate_featured_image($tweet->retweeted_status->entities->media[0]->media_url_https, $new_post);
                    endif;

                else :
                    // Handle normal tweets
                    $post = array(
                        'post_type'     => 'fx_twitter',
                        'post_content'  => htmlentities($tweet->text),
                        'post_excerpt'  => htmlentities($tweet->text),
                        'post_date'     => date("Y-m-d H:i:s", strtotime($tweet->created_at)),
                        'post_date_gmt' => date("Y-m-d H:i:s", strtotime($tweet->created_at)),
                        'post_status'   => 'publish',
                        'post_title'    => $tweet->id,
                        'post_name'     => $tweet->id,
                        'meta_input'    => array(
                            'fx_twitter_id' => $tweet->user->id,
                            'fx_twitter_name' => $tweet->user->name,
                            'fx_twitter_screen_name' => $tweet->user->screen_name,
                            'fx_twitter_profile_image' => $tweet->user->profile_image_url_https
                        )
                    );

                    $new_post = wp_insert_post( $post, true );

                    wp_set_object_terms($new_post, $tweet->user->screen_name, 'fx_screen_names');

                    // If there is an image set it as the featured image on the new post
                    if (isset($tweet->entities->media) && $new_post !== 0) :
                        fx_twitter_generate_featured_image($tweet->entities->media[0]->media_url_https, $new_post);
                    endif;
                endif;
            endif;
        endforeach;
    endforeach;
}

// Saves the image from the url then sets it to the post as the featured image
function fx_twitter_generate_featured_image( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);

    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1 = wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2 = set_post_thumbnail( $post_id, $attach_id );
}

// Conditional used to check if the post exists before we add it.
function fx_twitter_slug_exists($post_name) {
    global $wpdb;

    if ($wpdb->get_row("SELECT post_name FROM " . $wpdb->prefix . "posts WHERE post_name = '" . $post_name . "'", 'ARRAY_A')) :
        return true;
    else :
        return false;
    endif;
}

// Removes all FX Twitter Plugin settings
function fx_twitter_clear_settings() {
    delete_option('fx_twitter_consumer_key');
    delete_option('fx_twitter_consumer_secret');
    delete_option('fx_twitter_access_token');
    delete_option('fx_twitter_screen_names');
}

// Function to define settings necessary for admin page
global $plugin_url;
function fx_twitter_admin_init() {

    // Set plugin url
    $plugin_url = get_admin_url( null, 'options-general.php?page=fx_twitter_plugin_page' );

    // Check if reset button has been clicked
    if (isset($_GET['tw_action']) && $_GET['tw_action'] == 'unlink') :
        fx_twitter_clear_settings();
        wp_redirect($plugin_url);
    endif;

    // Create a settings group to store each setting in
    add_settings_section('fx_twitter_settings', 'Setting up the Twitter plugin', 'fx_twitter_render_settings_general', 'fx_twitter_plugin_page');

    add_settings_field('fx_twitter_consumer_key', 'Twitter App Details', 'fx_twitter_render_consumer_details', 'fx_twitter_plugin_page', 'fx_twitter_settings');

    // Define setting for the consumer key
    register_setting('fx_twitter_settings', 'fx_twitter_consumer_key', 'fx_twitter_validate_consumer_key' );

    // Define setting for the consumer secret
    register_setting('fx_twitter_settings', 'fx_twitter_consumer_secret', 'fx_twitter_validate_consumer_secret' );

    // Define setting for the access token
    register_setting('fx_twitter_settings', 'fx_twitter_access_token', 'fx_twitter_validate_access_token' );

    // Create a settings group to store twitter screen names
    add_settings_field('fx_twitter_screen_names', 'Screen Names', 'fx_twitter_render_user_details', 'fx_twitter_plugin_page', 'fx_twitter_settings');

    // Define settings for the user details (screen names)
    register_setting('fx_twitter_settings', 'fx_twitter_screen_names', 'fx_twitter_validate_access_token' );

    // Get access_token from twitter OAuth2
    if (isset($_GET['tw_action']) && $_GET['tw_action'] == 'get_token') :
        $consumer_key = get_option('fx_twitter_consumer_key');
        $consumer_secret = get_option('fx_twitter_consumer_secret');

        if ($consumer_key && $consumer_secret) :
            $base64_client = base64_encode($consumer_key . ':' . $consumer_secret);

            $ch = curl_init( 'https://api.twitter.com/oauth2/token' );
            curl_setopt( $ch, CURLOPT_POST, 1);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt( $ch, CURLOPT_HEADER, 0);
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Basic ' . $base64_client,
                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8.'
                )
            );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

            $response = json_decode(curl_exec( $ch ));

            if (!is_object($response) && $response->token_type !== 'bearer') :
                global $niw_twitter_message;
                $niw_twitter_message = array(
                    'type' => 'error',
                    'msg' => 'Error linking account: ' . $response
                );
            else :
                // Success, save our access token
                update_option('fx_twitter_access_token', $response->access_token ?: '', true);
            endif;
        endif;

        wp_redirect($plugin_url);
    endif;
}

// Functions to validate settings
function fx_twitter_validate_consumer_key($input) {
    $output = sanitize_text_field ( strip_tags( stripslashes( $input ) ) );

    return $output;
}
function fx_twitter_validate_consumer_secret($input) {
    $output = sanitize_text_field ( strip_tags( stripslashes( $input ) ) );

    return $output;
}
function fx_twitter_validate_access_token($input) {
    // Can't sanitize_text_field() on the access token as it removes special characters that the token needs
    $output = strip_tags( stripslashes( $input ) );

    return $output;
}

// Options page
function fx_twitter_admin_menu() {
    add_options_page('FX Twitter Plugin', 'FX Twitter Plugin', 'manage_options', 'fx_twitter_plugin_page', 'fx_twitter_render_options_page');
}

function fx_twitter_render_options_page() {
    ?>
        <div class="wrap">
            <h1>FX Twitter Plugin Options</h1>

            <form method="post" action="options.php">
                <?php
                    settings_fields('fx_twitter_settings');
                    do_settings_sections('fx_twitter_plugin_page');

                    submit_button();
                ?>
            </form>

            <h3>Shortcode usage</h3>
            <p>To display the Tweets on the front-end of the site, you are able to use a shortcode which has different display options.</p>
            <p><code>[fx_twitter tweets="3" images="default|only|none" retweets="show|hide"]</code></p>
            <p>The <code>tweets</code> option defines how many tweets to show.</p>
            <p>The <code>images</code> option defines whether to include tweets that do/ do not have images. <code>default</code> will show mixed, <code>only</code> will only show Tweets with an image and <code>none</code> will only show Tweets with no images.</p>
            <p>The <code>retweets</code> option defines whether to include tweets that do/ do not have images. <code>show</code> will only show retweets mixed with the main accounts Tweets and <code>hide</code> will only show Tweets from the main account.</p>
        </div>
    <?php
}

// Render each settings section on page
function fx_twitter_render_settings_general() {
    $plugin_url = get_admin_url( null, 'options-general.php?page=fx_twitter_plugin_page' );

    echo 'Before grabbing your Tweets, you must first authenticate your account. To do this, you will need to create a Twitter app and then login with your Twitter account. Just follow the instructions below!';

    ?>

    <h3 style="margin-top: 40px;">Step One: Register a new application client</h3>
    <p>You will need to head over to the <a href="https://apps.twitter.com/" target="_blank">Twitter Apps Portal</a> and click the <span style="color:#3d8b5f;border:1px dotted #3d8b5f;">Create New App</span> button. You may have to login with your Twitter account first.</p>
    <p>Fill in all the details the form asks for and click register. In the box for <strong>Callback URL</strong> you will need to include the following URL: <input value="<?php echo $plugin_url; ?>" type="text" readonly="readonly" style="display:block;margin:15px;width: 50%;padding:0.5em;"/></p>
    <p>Click <span style="color:#3d8b5f;border:1px dotted #3d8b5f;">Create your Twitter application</span> to finish creating the app, after which you will be taken to the Manage Apps screen.</p>


    <h3 style="margin-top: 40px;">Step Two: Get the app details</h3>
    <p>From the <a href="https://apps.twitter.com/" target="_blank">Manage Apps</a> screen, click on the app you just created.</p>
    <p>You will be presented with some details in the form of long alphanumeric codes that we will use to contact Twitter.</p>
    <p>Copy and paste the <strong>Consumer Key (API Key)</strong> and <strong>Consumer Secret (API Secret)</strong> into the respective boxes below.</p>
    <p>Next click the <span style="color:gray;border:1px dotted gray;">Submit app details</span> button below which will refresh this page and cause the <span style="color:gray;border:1px dotted gray;">Login with Twitter</span> button to appear below the Consumer Key and Secret.</p>
    <p>Click this button and login with your Twitter account. You will then be redirected back to this page, and everything will be all set-up!</p>

    <?php
}

// Display API details
function fx_twitter_render_consumer_details() {
    global $niw_twitter_message;
    $plugin_url      = get_admin_url( null, 'options-general.php?page=fx_twitter_plugin_page' );
    $consumer_key    = get_option('fx_twitter_consumer_key');
    $consumer_secret = get_option('fx_twitter_consumer_secret');
    $access_token    = get_option('fx_twitter_access_token');

    $auth_url = $plugin_url.'&tw_action=get_token';


    // Display and grab client ID input
    echo '<span style="display:inline-block;width: 125px;">Consumer Key (API Key):</span> <input name="fx_twitter_consumer_key" type="text" value="' . $consumer_key . '" style="width:50%;padding: 0.4em;" /><br>';
    // Display and grab client secret input
    echo '<span style="display:inline-block;width: 125px;">Consumer Secret (API Secret):</span> <input name="fx_twitter_consumer_secret" type="text" value="' . $consumer_secret . '" style="width:50%;padding: 0.4em;" /><br>';

    if (!$consumer_key && !$consumer_secret) :
        submit_button(
            __( 'Submit app details', 'plugin_domain' ),
            'secondary',
            'submit'
        );
    endif;

    if ($consumer_key  && $consumer_secret && !$access_token) :
        echo '<a class="button button-secondary" href="'.$auth_url.'" style="margin:0.5rem 0;">Login with Twitter</a><br><br>';
    endif;

    if ($access_token) :
        echo '<span style="display:inline-block;width: 128.5px;">Access Token:</span><input name="fx_twitter_access_token" type="text" value="' . $access_token . '" style="width:50%;padding: 0.4em;" /><br>';
    endif;

    if ($niw_twitter_message) :
        echo '<p style="color: #FF0000;">' . $niw_twitter_message['msg'] . '</p>';
    endif;
}

// Creates terms for the screen names
function fx_insert_terms($screen_names) {
    $screen_names = explode(',', $screen_names);

    // Loop through each screen name and check if it exists as a term
    foreach ($screen_names as $screen_name) :
        // Remove space before screen name if it exists
        $screen_name = trim($screen_name);

        // Check if term exists
        $term = term_exists($screen_name, 'fx_screen_names');
        if ($term === 0 || $term === null) :
            wp_insert_term($screen_name, 'fx_screen_names');
        endif;
    endforeach;
}

// Display user details
function fx_twitter_render_user_details() {
    $plugin_url = get_admin_url( null, 'options-general.php?page=fx_twitter_plugin_page' );
    $screen_names = get_option('fx_twitter_screen_names');

    $reset_url = $plugin_url . '&tw_action=unlink';
    ?>

    <p>Below add the screen names (without '@') of the Twitter accounts you want to pull tweets from, separate each screen name with a <strong>comma</strong>.</p>
    <p>For example the FX Digital twitter screen name (@wearefxdigital) would be <strong>wearefxdigital</strong>.</p><br>

    <?php
    echo '<span style="display:inline-block;width: 125px;">Screen Names:</span> <input type="text" name="fx_twitter_screen_names" value="'. $screen_names .'" style="width:50%;padding: 0.4em;"/>';

    if (!empty($screen_names)) :
        fx_insert_terms($screen_names);
        fx_twitter_create_import_schedule();
    endif;
}

// Shortcode definition
function fx_twitter($atts) {
    $atts = shortcode_atts(
        array(
            'tweets'      => 3,
            'taxonomy'    => false,
            'screen_name' => null,
            'images'      => null,  // only|none|null
            'retweets'    => 'show' // show|hide
        ),
        $atts
    );

    if (($atts['screen_name']) == null) :
        $terms = get_terms( array(
            'taxonomy'   => 'fx_screen_names',
            'hide_empty' => true,
        ));

        $atts['screen_name'] = $terms[0]->term_id;
    endif;

    $args = array(
        'post_type'      => 'fx_twitter',
        'post_status'    => 'publish',
        'orderby'        => 'post_date',
        'order'          => 'DESC',
        'posts_per_page' => $atts['tweets']
    );

    // If screen name is set query for fx_screen_names taxonomy
    if ($atts['taxonomy'] !== false && $atts['screen_name'] !== null) :
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'fx_screen_names',
                'field'    => 'term_id',
                'terms'    => $atts['screen_name']
            )
        );
    endif;

    // Filter for tweets with images only
    if ($atts['images'] == 'only') :
        if (empty($args['meta_query'])) :
            $args['meta_query'] = array();
        endif;

        array_push($args['meta_query'], array(
            'key'     => '_thumbnail_id',
            'compare' => 'EXISTS'
        ));
    endif;

    // Filter for tweets without images only
    if ($atts['images'] == 'none') :
        if (empty($args['meta_query'])) :
            $args['meta_query'] = array();
        endif;

        array_push($args['meta_query'], array(
            'key'     => '_thumbnail_id',
            'compare' => 'NOT EXISTS'
        ));
    endif;

    // Filtering out retweets by comparing the screen names
    if ($atts['retweets'] == 'hide') :
        if (empty($args['meta_query'])) :
            $args['meta_query'] = array();
        endif;

        array_push($args['meta_query'], array(
            'key'     => 'fx_twitter_screen_name',
            'value'   => get_term_by('id', $atts['screen_name'], 'fx_screen_names')->slug,
            'compare' => '='
        ));
    endif;

    $query = new WP_Query( $args );

    // Get the taxonomy object
    $taxonomy_screen_name = get_term_by('id', $atts['screen_name'], 'fx_screen_names');

    // Setup return string
    $return = '';

    // Build the tweets
    if ($query->have_posts()) :
        $return .= '<div class="fx-tweets">';
            while ($query->have_posts()) : $query->the_post();
                $post_meta = get_post_meta($query->post->ID);

                $return .= '<div class="fx-tweet">';

                    // Check if an image exists
                    if (has_post_thumbnail($query->post->ID)) :
                        $return .= '<div class="fx-tweet-image" style="background-image: url('. get_the_post_thumbnail_url($query->post->ID) .')">';
                        $return .= '</div>';
                    endif;

                    $return .= '<div class="fx-tweet-content">';
                        // Check if tweet is a retweet
                        if ($post_meta['fx_twitter_screen_name'][0] !== $taxonomy_screen_name->slug) :
                            $return .= '<div class="fx-retweet-text">';
                                $return .= '<span><a href="https://www.twitter.com/'. $taxonomy_screen_name->name . '" target="_blank" rel="noopener noreferrer">@'. $taxonomy_screen_name->name. '</a> retweeted</span>';
                            $return .= '</div>';
                        endif;

                        $return .= '<div class="fx-tweet-header">';
                            // The user details
                            $return .= '<div class="fx-tweet-user">';
                                $return .= '<div class="fx-tweet-avatar">';
                                    $return .= '<a href="https://www.twitter.com/'. $post_meta['fx_twitter_screen_name'][0] .'" target="_blank" rel="noopener noreferrer">';
                                        $return .= '<img src="'. $post_meta['fx_twitter_profile_image'][0] .'" alt="'. $post_meta['fx_twitter_name'][0] .'">';
                                    $return .= '</a>';
                                $return .= '</div>';

                                $return .= '<div class="fx-tweet-name">';
                                    $return .= '<a href="https://www.twitter.com/'. $post_meta['fx_twitter_screen_name'][0] .'" target="_blank" rel="noopener noreferrer">';
                                        $return .= '<h3 class="fx-tweet-full-name">'. $post_meta['fx_twitter_name'][0] .'</h3>';
                                    $return .= '</a>';

                                    $return .= '<a href="https://www.twitter.com/'. $post_meta['fx_twitter_screen_name'][0] .'" target="_blank" rel="noopener noreferrer">';
                                        $return .= '<span class="fx-tweet-screen-name">@'. $post_meta['fx_twitter_screen_name'][0] .'</span>';
                                    $return .= '</a>';
                                $return .= '</div>';
                            $return .= '</div>';

                            // Twitter logo
                            $return .= '<div class="fx-tweet-twitter-logo">';
                                $return .= '<svg id="Logo_FIXED" data-name="Logo — FIXED" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 400"><defs><style>.cls-1{fill:none;}.cls-2{fill:#1da1f2;}</style></defs><title>Twitter_Logo_Blue</title><rect class="cls-1" width="400" height="400"/><path class="cls-2" d="M153.62,301.59c94.34,0,145.94-78.16,145.94-145.94,0-2.22,0-4.43-.15-6.63A104.36,104.36,0,0,0,325,122.47a102.38,102.38,0,0,1-29.46,8.07,51.47,51.47,0,0,0,22.55-28.37,102.79,102.79,0,0,1-32.57,12.45,51.34,51.34,0,0,0-87.41,46.78A145.62,145.62,0,0,1,92.4,107.81a51.33,51.33,0,0,0,15.88,68.47A50.91,50.91,0,0,1,85,169.86c0,.21,0,.43,0,.65a51.31,51.31,0,0,0,41.15,50.28,51.21,51.21,0,0,1-23.16.88,51.35,51.35,0,0,0,47.92,35.62,102.92,102.92,0,0,1-63.7,22A104.41,104.41,0,0,1,75,278.55a145.21,145.21,0,0,0,78.62,23"/></svg>';
                            $return .= '</div>';
                        $return .= '</div>';

                        // The Tweet content
                        $return .= '<div class="fx-tweet-body">';
                            $return .= '<p>'. fx_twitter_parse_tweet(get_the_content()) . '</p>';
                        $return .= '</div>';

                        // The Tweet timestamp & actions
                        $return .= '<div class="fx-tweet-footer">';
                            // Timestamp
                            $return .= '<a href="https://www.twitter.com/'. $post_meta['fx_twitter_screen_name'][0] .'/status/'. $query->post->post_title .'" target="_blank" rel="noopener noreferrer" class="fx-tweet-timestamp">'. date('g:i A - j M Y', strtotime($query->post->post_date)) .'</a>';

                            // Tweet actions
                            $return .= '<ul class="fx-tweet-actions">';
                                // Reply
                                $return .= '<li class="fx-tweet-action reply">';
                                    $return .= '<a href="https://twitter.com/intent/tweet?in_reply_to='. $query->post->post_title .'">';
                                        $return .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 72"><path d="M41 31h-9V19c0-1.14-.647-2.183-1.668-2.688-1.022-.507-2.243-.39-3.15.302l-21 16C5.438 33.18 5 34.064 5 35s.437 1.82 1.182 2.387l21 16c.533.405 1.174.613 1.82.613.453 0 .908-.103 1.33-.312C31.354 53.183 32 52.14 32 51V39h9c5.514 0 10 4.486 10 10 0 2.21 1.79 4 4 4s4-1.79 4-4c0-9.925-8.075-18-18-18z"/></svg>';
                                    $return .= '</a>';
                                $return .= '</li>';

                                // Retweet
                                $return .= '<li class="fx-tweet-action retweet">';
                                    $return .= '<a href="https://twitter.com/intent/retweet?tweet_id='. $query->post->post_title .'">';
                                        $return .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 75 72"><path d="M70.676 36.644C70.166 35.636 69.13 35 68 35h-7V19c0-2.21-1.79-4-4-4H34c-2.21 0-4 1.79-4 4s1.79 4 4 4h18c.552 0 .998.446 1 .998V35h-7c-1.13 0-2.165.636-2.676 1.644-.51 1.01-.412 2.22.257 3.13l11 15C55.148 55.545 56.046 56 57 56s1.855-.455 2.42-1.226l11-15c.668-.912.767-2.122.256-3.13zM40 48H22c-.54 0-.97-.427-.992-.96L21 36h7c1.13 0 2.166-.636 2.677-1.644.51-1.01.412-2.22-.257-3.13l-11-15C18.854 15.455 17.956 15 17 15s-1.854.455-2.42 1.226l-11 15c-.667.912-.767 2.122-.255 3.13C3.835 35.365 4.87 36 6 36h7l.012 16.003c.002 2.208 1.792 3.997 4 3.997h22.99c2.208 0 4-1.79 4-4s-1.792-4-4-4z"/></svg>';
                                    $return .= '</a>';
                                $return .= '</li>';

                                // Like
                                $return .= '<li class="fx-tweet-action like">';
                                    $return .= '<a href="https://twitter.com/intent/like?tweet_id='. $query->post->post_title .'">';
                                        $return .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 72"><path d="M38.723,12c-7.187,0-11.16,7.306-11.723,8.131C26.437,19.306,22.504,12,15.277,12C8.791,12,3.533,18.163,3.533,24.647 C3.533,39.964,21.891,55.907,27,56c5.109-0.093,23.467-16.036,23.467-31.353C50.467,18.163,45.209,12,38.723,12z"/></svg>';
                                    $return .= '</a>';
                                $return .= '</li>';

                            $return.= '</ul>';
                        $return .= '</div>';
                    $return .= '</div>';
                $return .= '</div>';
            endwhile;
        $return .= '</div>';

        // Resotre original post data
        wp_reset_postdata();
    else :
        // No tweets
    endif;

    return $return;
}
add_shortcode( 'fx_twitter', 'fx_twitter' );

// Stylesheet
// Enqueue style sheet.
add_action( 'wp_enqueue_scripts', 'fx_twitter_register_plugin_styles' );

/**
 * Register style sheet.
 */
function fx_twitter_register_plugin_styles() {
    wp_register_style( 'fx-twitter-posts-plugin', plugins_url( 'fx-twitter-posts-plugin/plugin.css' ) );
    wp_enqueue_style( 'fx-twitter-posts-plugin' );
}

add_action( 'admin_init', 'fx_twitter_admin_init');
add_action( 'admin_menu', 'fx_twitter_admin_menu');

?>
