## About

This is a WordPress plugin that pulls in tweets from the Twitter API and converts them into posts.

## Changelog
    - ** 1.0.7 Jun. 28th 2018 - Enhancements for the plugin. **
        - Improved the filtering for tweets with/without images.
        - Improved the filtering for tweets that are/aren't retweets.

    - ** 1.0.6 Nov. 9th 2017 - Now saving retweets as a pos t**
        - Retweets from the selected accounts are now being saved as posts of thier own.
        - Setting since_id on a per account basis.

    - ** 1.0.5 Sep. 15th 2017 - Add new meta query **
        - Allow users to filter for tweets with images only.

    - ** 1.0.4 Sep. 15th 2017 - Create screen name taxonomy **
        - Created a taxonomy called fx_screen_names to store the twitter screen names.
        - Changed the meta query to a taxonomy query.

    - ** 1.0.3 Aug. 29th 2017 - Fix prod bug **
        - Pulling in profile images over https.

    - ** 1.0.2 Aug. 23rd 2017 - Minor updates **
        - Loading images over https.
        - Added ability to query for screen names.

    - ** 1.0.1 Aug. 21st 2017 - Add Images **
        - Adding the attached images to the tweets.

    - ** 1.0.0 Aug. 21st 2017 - FX Twitter Posts **
        - The plugin pulls in tweets using the Twitter API every hour and converts them into posts with the post_type 'fx_twitter'.
        - In order for the plugin to work it takes in:
            - Consumer Key
            - Consumer Secret
            - Screen names
